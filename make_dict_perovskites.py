#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  5 20:01:02 2022

@author: joaquin
"""

## Load modules, exploratory data and cleansing ##

import pandas as pd
import json


df = pd.read_csv("Perovskite_database_content_all_data.csv", 
                 sep=",", 
                 engine='python')

print(df.head())

# Remove the two first (apparently) unnecessary columns.
df.drop(['Ref_ID', 'Ref_ID_temp'], axis=1,  inplace=True)

# Check the number of entries
print(len(df))

# Overview missing data
print(df.isnull().sum())

# Create list holding the names of the 408 columns
list_col = df.columns

# Quick check of column name with slicing and search formula
print(list_col[0:50])
print("=========================================")
print(list_col[50:100])
print("=========================================")
print(list_col[100:150])
print("=========================================")


# Relevant information in columns related to the Perovskites formulas.
print(list_col[74:86])

## Choosing formula representation and prepare data ##

# Looking at Perovskites formulas in short notation
df["Perovskite_composition_short_form"]
# Create list holding the (447) different 'short_formulas'
df_short =  df[df['Perovskite_composition_short_form'].notna()]
compositions_short = df_short['Perovskite_composition_short_form'].unique()

# Count number of missing entries with "Perovskite_composition_long_form" info
print(len(df.loc[df['Perovskite_composition_long_form'].isnull(), :]))

# Looking at one raw with missing formula and thinking about a possible solution
print(df.loc[42936:42936,['Perovskite_composition_perovskite_ABC3_structure',
                    'Perovskite_composition_a_ions',
                    'Perovskite_composition_b_ions',
                    'Perovskite_composition_c_ions',
                    "Perovskite_composition_short_form",
                    "Perovskite_composition_long_form",
                    'Perovskite_composition_inorganic']])

# Remove the 30 raws  missing 'Perovskite_composition_long_form'.
df_long =  df[df['Perovskite_composition_long_form'].notna()]
print(df_long['Perovskite_composition_long_form'].isnull().sum())

# Create list holding the (2138) different 'long_formulas'
compositions_long = df_long['Perovskite_composition_long_form'].unique()
print(f" The instances for 'short_form'  is {len(compositions_short)}; The instances for 'long_form ' is {len(compositions_long)}") 

# A glance into 'Perovskite_composition_long_form'
print( df_long.iloc[[0],:] )

# Example how to select information with rows fullfilling a specific 'long_form" condition, here 'CsSnI3
df_test = df_long.loc[df_long.loc[:,'Perovskite_composition_long_form']=='CsSnI3']
print (df_test.head() )
del df_test

"""
Main routine to create dictionary 

Strategy: The dictionary 'perovskites' contains keys corresponding to the 
formulas of the Perovskites. These keys are set when the 'Perovskite_composition_long_form' 
condition is applied to the list 'compositions_long'. Once the formula is selected, 
two nesting loops create two dictionaries. The innerest loops goes over the 408
 columns and create from them keys with their correspinding column values the 
 first dictoinary (index_dict). This inner loop is inside an outer loop which 
 in turn goes through all raws of the data frame 'df_temp' which contains all 
 instances of the chosen 'Perovskite_composition_long_form'. Finally, the 
 resulting dictionary ('comp_dict') with all formula instances is appended to 
 the main dictionary 'perovskites'.
 
Note: For the outer loop one can either use the variable 'count' to create the 
keys of all instances in 'comp_dict', or use the 'index' of the 'df_temp'. 
Whe using the second alternative, one must apply "df_temp.reset_index()" 
to secure to index over 0, 1, 2, 3 .

"""

perovskites = {}                           # NAME OF MAIN DICTIONARY

for comp in compositions_long:
    comp_dict = comp 
    comp_dict = {}                         # Keys of 'perovskites' dictionary. The keys are the formulas.
    df_temp = df_long.loc[df_long.loc[:,'Perovskite_composition_long_form']==comp] # formula "long_form" condition.
    df_temp = df_temp.reset_index()                                                # Taking care of new indexes
    df_temp.drop(['index'], axis=1,  inplace=True)                                 # Remove old indexes
#    count = 0
    comp_dict["instances"] = len(df_temp)   # The keys 'comp_dict' sub-dictionary contain information 
    for index, row in df_temp.iterrows():   # about the number of intances for a given formula.
        index_dict = str(index) + "_" + "dict"
        index_dict = {}                     # 'index_dict' is key as sub-dictionary for 'comp_dict'. In total 
        for column in df_temp:              # there are len(df_temp) such keys for each instance formula('comp_dict').
            key_column = column             
            val_column = row[column]
            index_dict[key_column] = row[column]
#        print (index, count)
        comp_dict[index] = index_dict       # Enter the sub-dictionary 'index_dict' with its 408 keys into comp_dict  
#        comp_dict[count] = index_dict
#        count = count + 1  
    print(f" The instances for {comp} is {len(df_temp)} ")
    perovskites[comp] = comp_dict           # Enter 'comp_dict' into main dictionary
    
print("Perovskite dictionary is ready!")


## Test the dictionary ##

print (perovskites.get("CsSnBr2I"))
print("=========================================")
print(perovskites["CsSnBr2I"][2])
print("=========================================")
print(perovskites["CsSnBr2I"][2]["Cell_stack_sequence"])
print("=========================================")
print(perovskites["CsSnI3"][70])
print("=========================================")
print(perovskites["Cs0.15FA0.55MA0.3PbBr0.6I2.4"][0]["Perovskite_band_gap"])
print("=========================================")

for formula in perovskites.keys():
    print(formula)

count = 0
for comp in compositions_long:
    count =count + len(perovskites[comp])-1
    
print(f" The total instances {count}; original entries {len(df)-30} ")

## Save as json format ##
with open('dict_perovskites.json', 'w') as fp:
    json.dump(perovskites, fp, indent = 4)




