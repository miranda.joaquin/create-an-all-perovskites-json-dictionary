# Create an all-Perovskites JSON dictionary



## Getting started

Generation of a dictionary with the formulas of the perovskites as keys. As values, the dictionaty has sub-dictionaries for each perovskite composition containing all properties for this specific perovskite material.  

This project includes the python code to create a JSON dictionary for 42911 Perovskites based on their chemical formula. The dictionary contains up to 408 properties.
